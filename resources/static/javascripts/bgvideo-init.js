/*global $:false */
/*global window: false */

(function () {
    "use strict";
    $(function ($) {
        if (!device.tablet() && !device.mobile()) {
            $.okvideo({
                source: '81676731', //set your video source here
                autoplay: true,
                loop: true,
                highdef: true,
                hd: true,
                adproof: true,
                volume: 0 // control the video volume by setting a value from 0 to 99
            });
        } else {
            $('.intro-video-bg').addClass('poster-img');
        }
    });
})();