from django.contrib import admin
from django.conf import settings
from feincms.admin import tree_editor

from flow.models import *


class NodeAdmin(admin.ModelAdmin):
    list_display = ['id', 'content']
    list_editable = ('content',)


class ArrowAdmin(admin.ModelAdmin):
    list_display = ['up', 'content', 'down']
    list_editable = ('content',)


admin.site.register(Node, NodeAdmin)
admin.site.register(Arrow, ArrowAdmin)
