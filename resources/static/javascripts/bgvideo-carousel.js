/*global $:false */
/*global window: false */

(function () {
  "use strict";
  $(function ($) {
    if ($('.videoClass').length) {
      var is_mobile = (device.tablet() || device.mobile()) ? true : false;
      var src = (is_mobile) ? $('.videoClass').attr('data-mlink') : $('.videoClass').attr('data-link');
      if (src) {
        $('#home-carousel-wrap').okvideo({
          source: src,
          controls: false,
          highdef: true,
          adproof: false,
          annotations: false,
          loop: (is_mobile) ? 0 : 1,
          volume: 0,
          onReady: video_onReady,
          onPlay: video_onPlay,
          onPause: video_onPause,
          buffering: video_buffer,
          screenChangeCall: true,
          hd: (is_mobile) ? 0 : 1
        });
      }
    } else {
      carousel_init();
    }
  });
})();