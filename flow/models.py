from django.db import models
from simplemde.fields import SimpleMDEField


class Node(models.Model):
    is_top = models.BooleanField(verbose_name='최상위 ?', default=False)
    content = models.CharField(verbose_name='질문', max_length=200)
    sub = models.CharField(verbose_name='부제', max_length=300, null=True, blank=True)
    explain = SimpleMDEField(verbose_name='설명', null=True, blank=True)

    def __str__(self):
        return self.content


class Arrow(models.Model):
    up = models.ForeignKey(Node, verbose_name='이전 단계', related_name='parent')
    content = models.CharField(verbose_name='대답', max_length=200)
    down = models.ForeignKey(Node, verbose_name='다음 단계', related_name='children', null=True, blank=True)

    def __str__(self):
        return self.content
