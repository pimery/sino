/*global $:false */
/*global window: false */

(function () {
  "use strict";
  $(function ($) {
    var collection = $('.videoClass');
    var is_mobile = device.tablet() || device.mobile();
    collection.each(function (index) {
      var src = (is_mobile) ? $(this).attr('data-mlink') : $(this).attr('data-link');
      if (src) {
        var option = {
          source: src,
          controls: false,
          highdef: true,
          adproof: false,
          annotations: false,
          loop: !is_mobile,
          volume: 0,
          playsinline: is_mobile,
          hd: !is_mobile,
          newtarget: $(this).attr('id')
        };
        $(this).okvideo(option);
      }
    });
  });
})();