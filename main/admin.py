from django.contrib import admin
from markdownx.widgets import AdminMarkdownxWidget

from main.models import *


class CarouselInline(admin.StackedInline):
    model = CarouselItem
    extra = 1


class FilmInline(admin.TabularInline):
    model = FilmItem
    extra = 0


class ArticleCarousel(Article):
    objects = CarouselManager()

    class Meta:
        proxy = True


class ArticleFilm(Article):
    objects = FilmManager()

    class Meta:
        proxy = True


class ArticleSns(Article):
    objects = SnsManager()

    class Meta:
        proxy = True


class ArticleAdmin(admin.ModelAdmin):
    list_display = ['title', 'category', 'publish', 'date']
    list_editable = ('publish',)
    formfield_overrides = {
        models.TextField: {'widget': AdminMarkdownxWidget},
    }


class ArticleCarouselAdmin(ArticleAdmin):
    inlines = (CarouselInline,)

    def get_form(self, request, obj=None, **kwargs):
        form = super(ArticleCarouselAdmin, self).get_form(request, obj, **kwargs)
        form.base_fields['category'].queryset = Category.objects.filter(temp__file='carousel.html')
        return form


class ArticleFilmAdmin(ArticleAdmin):
    inlines = (FilmInline,)

    def get_form(self, request, obj=None, **kwargs):
        form = super(ArticleFilmAdmin, self).get_form(request, obj, **kwargs)
        form.base_fields['category'].queryset = Category.objects.filter(temp__file='film.html')
        return form


class ArticleSnsAdmin(ArticleAdmin):
    def get_form(self, request, obj=None, **kwargs):
        form = super(ArticleSnsAdmin, self).get_form(request, obj, **kwargs)
        form.base_fields['category'].queryset = Category.objects.filter(temp__file='sns.html')
        return form


# admin.site.register(Article, ArticleAdmin)
admin.site.register(ArticleCarousel, ArticleCarouselAdmin)
admin.site.register(ArticleFilm, ArticleFilmAdmin)
admin.site.register(ArticleSns, ArticleSnsAdmin)
admin.site.register(Author)
admin.site.register(Category)
admin.site.register(Template)
admin.site.register(Info)
