/*global $:false */
/*global window: false */

(function () {
  "use strict";
  $(function ($) {
    var is_mobile = device.tablet() || device.mobile();
    var option = {
      adproof: false,
      annotations: false,
      highdef: true,
      playsinline: is_mobile,
      hd: !is_mobile,
      loop: false,
      onReady: film_onReady,
      onFinished: film_next,
      volume: 0
    };
    if (!is_mobile) {
      option.source = $('.film-video:first-child').attr('data-link');
    } else {
      option.source = $('.film-video:first-child').attr('data-mlink');
    }
    $('#mastwrap').okvideo(option);
  });
})();