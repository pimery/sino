from django.shortcuts import render, redirect
from django.shortcuts import render_to_response
from django.views.generic import *
from main.models import *


def index(request):
    return render_to_response('index.html', None)


def web_404(request):
    return render_to_response('404.html')


def web_500(request):
    return render_to_response('500.html')


class ArticleDetailView(DetailView):
    model = Article

    def get_object(self, queryset=None):
        if 'yy' in self.kwargs:
            article = Article.objects.get(date__year=self.kwargs['yy'],
                                          date__month=self.kwargs['mm'],
                                          date__day=self.kwargs['dd'],
                                          title=self.kwargs['title'])
            if article is not None:
                return article
            return render_to_response('index.html', None)
        return Article.objects.get(id=self.kwargs['pk'])

    def get_template_names(self):
        return 'detail_' + self.get_object().category.temp.file

    def get_context_data(self, **kwargs):
        context = super(ArticleDetailView, self).get_context_data(**kwargs)
        article = self.get_object()
        if isCategory('film', article.category):
            context['film'] = FilmItem.objects.filter(article=article).order_by('num')
        if isCategory('carousel', article.category):
            context['object_list'] = CarouselItem.objects.filter(article__publish=True, article__category=article.category).order_by('-article__date')
        return context


class ArticleListView(ListView):
    def get_queryset(self):
        category = Category.objects.get(name=self.kwargs['category'])
        self.template_name = category.temp.file
        if isCategory('carousel', category):
            return CarouselItem.objects.filter(article__publish=True, article__category=Category.objects.get(name=self.kwargs['category'])).order_by('-article__date')
        else:
            return Article.objects.filter(publish=True, category=category).order_by('-datetime')

    def dispatch(self, request, *args, **kwargs):
        category = Category.objects.get(name=self.kwargs['category'])
        if self.get_queryset().count() == 0:
            return render_to_response('error.html', context={'name': '준비중', 'mes': '현재 표시할 내용이 없습니다'})
        if isCategory('carousel', category) and self.get_queryset().exists():
            return redirect(self.get_queryset()[0].article.get_link())
        if isCategory('film', category) and self.get_queryset().count() == 1:
            return redirect(self.get_queryset()[0].get_link())
        return super(ArticleListView, self).dispatch(request, *args, **kwargs)


class InfoListView(ListView):
    template_name = 'index.html'

    def get_queryset(self):
        return Info.objects.order_by('name')


def isCategory(find, cate):
    return find in cate.temp.file
