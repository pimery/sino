from django.conf.urls import url
from flow.views import *

urlpatterns = [
    url(r'list$', FlowList.as_view(), name='flows'),
    url(r'^(?P<pk>\d+)$', FlowView.as_view(), name='flow'),
]