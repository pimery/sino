function setCoverImage() {
  var is_mobile = device.tablet() || device.mobile();
  var src;
  $('.cover-image').each(function () {
    if (is_mobile) {
      src = $(this).attr('data-msrc');
    } else {
      src = $(this).attr('data-src');
    }
    $(this).css('background-image', 'url(' + src + ')');
  });
  $('.videoClass').each(function () {
    if (is_mobile) {
      src = $(this).attr('data-mlink');
    } else {
      src = $(this).attr('data-link');
    }
    $(this).find('.videoCover').css('background-image', 'url("http://img.youtube.com/vi/' + src + '/0.jpg")');
  });
}

function isIOSweb() {
  return /(iPhone|iPod|iPad).*AppleWebKit(?!.*Safari)/i.test(window.navigator.userAgent);
}

function isIOS() {
  return /iPhone|iPod|iPad/.test(window.navigator.userAgent);
}

function isIE() {
  return (navigator.userAgent.indexOf('MSIE') !== -1 || navigator.appVersion.indexOf('Trident/') > 0);
}

function set_meta(author, image) {
  $('meta[property="og:title"]').attr('content', $('title').text());
  $('meta[name=author]').attr('content', author);
  if ($(image).attr('data-src')) {
    $('meta[property="og:image"]').attr('content', location.protocol + "//" + location.host + $(image).attr('data-src'));
  }
}

function youtube_cue($link) {
  videoCue((device.tablet() || device.mobile()) ? $link.attr('data-mlink') : $link.attr('data-link'));
}

function index_init() {
  $('.intro-article-wrap').each(function () {
    if ($(this).height() + 10 < $(this).parent().height()) {
      $(this).addClass('center-ver-hor');
      $(this).parent().css('height', $(window).height());
    }
  });

  var config1 = liquidFillGaugeDefaultSettings();
  config1.circleColor = "#C5FF6A";
  config1.textColor = "#C5FF6A";
  config1.waveTextColor = "#ffffff";
  config1.waveColor = "rgba(197, 255, 106, 0.5)";
  config1.circleThickness = 0.2;
  config1.textVertPosition = 0.2;
  config1.waveAnimateTime = 1000;
  config1.waveCount = 5;
  loadLiquidFillGauge("fillgauge1", 93.4, config1);
}

function ver_index_init() {
  var vH = $(window).height(),
      full = (typeof window.screen.height != 'undefined') ? Math.max(window.screen.height, vH) : vH,
      len = document.getElementsByClassName('info-article').length,
      intro = Math.max($('.intro-article').outerHeight(), vH),
      max = intro + full * (len - 1),
      min = intro - vH + full / 2 - $('.info-profile').height();
  $('.info-article-rel article:first-child').css('top', intro);
  $('#info-article-wrap').css('height', $('.info-article-rel').outerHeight());
  $('.fade-scroll').each(function () {
    $(this).css('opacity', 0);
  });
  var offset = 0, cur = -1;
  ver_index_fade(offset, cur, intro, full, max);
  $(window).bind('scroll', function () {
    offset = $(document).scrollTop();
    cur = Math.floor((offset - intro) / full);
    if (min < offset && offset < intro) {
      $('#first-profile').css('opacity', 1);
    } else {
      $('#first-profile').css('opacity', 0);
    }
    ver_index_fade(offset, cur, intro, full, max);
  });

  var config1 = liquidFillGaugeDefaultSettings();
  config1.circleColor = "#e0ff66";
  config1.textColor = "#e0ff66";
  config1.waveTextColor = "#ffffff";
  config1.waveColor = "rgba(224, 255, 102, 0.5)";
  config1.circleThickness = 0.2;
  config1.textVertPosition = 0.2;
  config1.waveAnimateTime = 1000;
  config1.waveCount = 5;
  loadLiquidFillGauge("fillgauge1", 93.4, config1);
}

function ver_index_fade(offset, cur, intro, full, max) {
  var opacity = (offset - intro - (full * cur)) / full;
  console.log('[' + offset + ']' + max);
  $('.fade-scroll').each(function (index) {
    if (offset < intro) {
      $(this).css('opacity', 0);
    } else if (cur >= max) {
      if (index == cur) $(this).css('opacity', 1);
      else $(this).css('opacity', 0);
    } else {
      if (index == cur) {
        $(this).css('opacity', 1 - opacity);
      } else if (index == (cur + 1)) {
        $(this).css('opacity', opacity);
      } else {
        $(this).css('opacity', 0);
      }
    }
  });
}

function film_item(clicked, $link) {
  $('.film-video').each(function (index) {
    if (clicked == index) {
      if (!$(this).hasClass('playing')) {
        $(this).addClass('playing');
      }
    } else {
      if ($(this).hasClass('playing')) {
        $(this).removeClass('playing');
      }
    }
  });
  youtube_cue($link);
  film_play(true);
}

function film_play(play) {
  var ctrl = $('#film-ctrl'),
      intro = $('.film-intro'),
      video = $('#okplayer');
  if (play) {
    if (intro.is(':visible')) {
      intro.hide();
      video.show();
    }
    ctrl.removeClass('fa-play').addClass('fa-pause');
    videoCtrl(OK_PLAY);
  } else {
    ctrl.removeClass('fa-pause').addClass('fa-play');
    videoCtrl(OK_PAUSE);
  }
}

function film_onReady() {
  $('#okplayer').attr('allowfullscreen', 0).hide();
}

function film_next() {
  film_play(false);
  $('.film-video.playing').closest('.owl-item').next().find('.film-video').click();
}

function film_init() {
  var ctrl = $('#film-ctrl');
  setCoverImage();
  if (isIOS() && !isIOS()) {
    $(window).resize(function () {
      clearTimeout(window.resizedFinished);
      window.resizedFinished = setTimeout(function () {
        location.reload();
      }, 10);
    });
  }
  $('#mastwrap').css('overflow', 'hidden');
  ctrl.click(function () {
    if ($(this).hasClass("fa-play")) {
      film_play(true);
    } else {
      film_play(false);
    }
  });
  $('.film-video').each(function (index) {
    $(this).bind("click", function () {
      film_item(index, $(this));
    });
  });
  $('.film-intro-play').click(function () {
    film_play(true);
  });
  set_meta($('.article-author').text(), '.cover-image');
}

function carousel_init() {
  setCoverImage();
  $(window).load(function () {
    $('body').removeClass('black-bg');
    $('#loader').hide();
    $('#mastwrap').css('opacity', 1);
    var index = $('.detail').attr('id');
    if (index == 0) {
      screenChanged(0);
    } else {
      $("#home-carousel").data('owlCarousel').jumpTo(index);
    }
  });
  $('#home-carousel').bind("touchmove", function (event) {
    event.preventDefault();
  });
  $('.scrolldown').each(function () {
    $(this).click(function () {
      var icon = $(this).find('i.fa');
      if (icon.hasClass('fa-angle-double-down')) {
        $('body').animate({scrollTop: $(window).height()}, "slow");
      } else {
        $('body').animate({scrollTop: 0}, "slow");
      }
    });
  });
  $(window).scroll(function () {
    if ($(window).scrollTop() == 0) {
      $('.detail').find('.scrolldown i.fa').removeClass('fa-angle-double-up').addClass('fa-angle-double-down');
    } else {
      $('.detail').find('.scrolldown i.fa').removeClass('fa-angle-double-down').addClass('fa-angle-double-up');
    }
  });
  $(document).keydown(function (e) {
    var owl = $("#home-carousel").data('owlCarousel');
    switch (e.which) {
      case 37:
        owl.prev();
        break;
      case 39:
        owl.next();
        break;
      case 38:
      case 40:
        $('#' + owl.currentItem).find('.scrolldown').click();
        break;
      default:
        return;
    }
    e.preventDefault();
  });
}

function video_onReady() {
  $('#okplayer').hide();
  carousel_init();
}

function video_onPlay() {
  $('#loader').hide();
  carousel_videoCover();
  $('#okplayer').show();
}

function video_onPause() {
  $('#okplayer').hide();
  carousel_videoCover();
}

function video_buffer() {
  $('#loader').show();
}

function carousel_videoCover() {
  $('.videoCover').each(function () {
    if ($(this).parent().hasClass('detail')) {
      $(this).hide();
    } else {
      $(this).show();
    }
  });
}

function carousel_set_video(pos) {
  if (!has_video(pos)) {
    videoCtrl(OK_PAUSE);
    return;
  }
  youtube_cue($('#' + pos));
  videoCtrl(OK_PLAY);
}

function screenChanged(pos) {
  $('.detail').removeClass('detail');
  $('#' + pos).addClass('detail');
  carousel_set_video(pos);
  var article;
  if (!isIE()) {
    window.history.pushState({}, '', $('#url' + pos).val());
  }
  for (var i = 0; i < $('.owl-item').length; i++) {
    var detail = $('#mastfoot' + i);
    if (i == pos) {
      article = detail;
      $(document).attr("title", detail.find('.article-title').text());
      detail.show();
    } else {
      detail.hide();
    }
  }
  $('body').animate({scrollTop: 0}, "slow");
  set_meta(article.find('.article-author').text(), '#' + pos);
}

function has_video(pos) {
  return $('#' + pos).hasClass('videoClass');
}

function flow_top_size() {
  $('.flow-top .welcome').css('margin-bottom', $('.flow-children').height());
}

function flow_size() {
  var maxHeight = Math.max.apply(null, $('.col-height').map(function () {
    return $(this).height();
  }).get());
  if (maxHeight) {
    $('.col-height').each(function () {
      $(this).find('.about-content-main').css('height', maxHeight);
    });
  }
  flow_top_size();
}

function flow_init() {
  flow_size();
  $(window).resize(flow_size);
}