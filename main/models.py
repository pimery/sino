from django.db import models
from datetime import date
from colorful.fields import RGBColorField
from markdownx.models import MarkdownxField
from simplemde.fields import SimpleMDEField


class Author(models.Model):
    name = models.CharField(verbose_name='작성자', max_length=100)

    def __str__(self):
        return self.name


class Template(models.Model):
    name = models.CharField(verbose_name='템플릿 이름', max_length=100)
    file = models.CharField(verbose_name='파일 이름', max_length=50)

    def __str__(self):
        return self.name


class Category(models.Model):
    name = models.CharField(verbose_name='카테고리 이름', max_length=100)
    temp = models.ForeignKey(Template)

    def __str__(self):
        return self.name


class Article(models.Model):
    author = models.ForeignKey(Author)
    title = models.CharField(verbose_name='제목', max_length=100)
    category = models.ForeignKey(Category, null=True)
    date = models.DateField(verbose_name='날짜', default=date.today)
    cover_img = models.ImageField(verbose_name='커버 이미지', upload_to='cover', null=True, blank=True)
    m_img = models.ImageField(verbose_name='모바일용 커버', upload_to='mobile', null=True, blank=True)
    # content = SimpleMDEField(verbose_name='내용')
    content = MarkdownxField(verbose_name='내용')
    datetime = models.DateTimeField(auto_now_add=True)
    edittime = models.DateTimeField(auto_now=True)
    publish = models.BooleanField(verbose_name='publish', default=False)

    def __str__(self):
        return self.title

    def get_link(self):
        link = '/' + str(self.date.year) + '/' + str(self.date.month) + '/' + str(self.date.day) + '/' + self.title
        return link

    def get_src(self):
        src = 'data-src=' + self.cover_img.url + ' data-msrc='
        if self.m_img:
            src += self.m_img.url
        else:
            src += self.cover_img.url
        return src


class CarouselItem(models.Model):
    article = models.OneToOneField(Article)
    subtitle = models.CharField(verbose_name='부제', max_length=100)
    cover_video = models.CharField(verbose_name='커버 영상', max_length=50, null=True, blank=True)
    m_video = models.CharField(verbose_name='모바일용 커버 영상', max_length=50, null=True, blank=True)
    link_btn_color = RGBColorField(verbose_name='하단 버튼 색', default='#ffffff')

    def __str__(self):
        return self.subtitle

    def get_link(self):
        return make_link(self.cover_video, self.m_video)


class FilmItem(models.Model):
    article = models.ForeignKey(Article)
    subtitle = models.CharField(verbose_name='부제', max_length=100)
    video = models.CharField(verbose_name='영상', max_length=50)
    m_video = models.CharField(verbose_name='모바일용 영상', max_length=50, null=True, blank=True)
    index = models.CharField(verbose_name='표시', max_length=2, default='영')
    num = models.IntegerField(verbose_name='순서', default=0)

    def __str__(self):
        return self.article.title + '-' + self.subtitle

    def get_link(self):
        return make_link(self.video, self.m_video)


class Info(models.Model):
    img = models.ImageField(verbose_name='프로필 이미지', upload_to='profile')
    name = models.CharField(verbose_name='이름', max_length=100)
    info = models.CharField(verbose_name='소개', max_length=200)
    email = models.EmailField(verbose_name='이메일')
    facebook = models.CharField(verbose_name='페이스북', max_length=100, null=True, blank=True)
    youtube = models.CharField(verbose_name='유투브', max_length=100, null=True, blank=True)
    color = RGBColorField(verbose_name='배경 색', default='#333333')

    def __str__(self):
        return self.name


class CarouselManager(models.Manager):
    def get_queryset(self):
        return super(CarouselManager, self).get_queryset().filter(category__temp__file='carousel.html')


class FilmManager(models.Manager):
    def get_queryset(self):
        return super(FilmManager, self).get_queryset().filter(category__temp__file='film.html')


class SnsManager(models.Manager):
    def get_queryset(self):
        return super(SnsManager, self).get_queryset().filter(category__temp__file='sns.html')


def make_link(origin, mobile):
    link = 'data-link=' + origin + ' data-mlink='
    if mobile:
        link += mobile
    else:
        link += origin
    return link
