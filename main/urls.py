from django.conf.urls import url
from main.views import *

urlpatterns = [
    url(r'^$', InfoListView.as_view(), name='home'),
    url(r'^(?P<category>\w+)$', ArticleListView.as_view(), name='category'),
    url(r'^(?P<yy>\d+)/(?P<mm>\d+)/(?P<dd>\d+)/(?P<title>.+)$', ArticleDetailView.as_view(), name='detail'),
]