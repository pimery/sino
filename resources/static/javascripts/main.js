// MAIN.JS
//--------------------------------------------------------------------------------------------------------------------------------
//This is main JS file that contains custom JS scipts and initialization used in this template*/
// -------------------------------------------------------------------------------------------------------------------------------
// Template Name: IDENTITY.
// Author: Designova.
// Version 1.0 - Initial Release
// Website: http://www.designova.net 
// Copyright: (C) 2014
// -------------------------------------------------------------------------------------------------------------------------------

/*global $:false */
/*global window: false */

(function () {
  "use strict";
  $(function ($) {
    var vH = $(window).height(), full = vH;
    if (device.tablet() || device.mobile()) {
      full = (typeof window.screen.height != 'undefined') ? Math.max(window.screen.height, vH) : vH;
    }
    $('.full-height,  body.pace-running').css('height', vH);
    $('.intro, #mastwrap').css('min-height', vH);
    $('.intro-article').css('min-height', full);
    $('.full-screen').css('height', full);
    $(window).load(function () {
      //$(function ($) {
      /*if your element is an image then please use $(window).load() instead tha above function wrap, because we want the coding to take
       effect when the image is loaded. */
      var parent_height = $('.vertical-center').parent().height();
      var image_height = $('.vertical-center').height();
      var top_margin = (parent_height - image_height) / 2;
      $('.vertical-center').css('padding-top', top_margin);
      $('.vertical-center-img').css('margin-top', top_margin);
    });
    $(window).stellar({
      responsive: true,
      horizontalScrolling: false,
      parallaxBackgrounds: true,
      parallaxElements: true,
      hideDistantElements: true
    });

    var option = {
      pagination: false,
      items: 1,
      itemsDesktop: [5000, 1],
      itemsDesktopSmall: [1440, 1],
      itemsTablet: [1024, 1],
      itemsTabletSmall: [640, 1],
      itemsMobile: [360, 1],
      autoHeight: true,
      autoPlay: false,
      screenChangeCall: true
    };
    if ((!device.tablet() && !device.mobile())) {// || (isIOS() && !isIOSweb())) {
      option.navigation = true;
      option.navigationText = [
        '<i class="fa fa-angle-left"></i>',
        '<i class="fa fa-angle-right"></i>'
      ];
    } else {
      option.navigation = false;
    }
    $("#home-carousel").owlCarousel(option);
    $("#home-carousel-film").owlCarousel({
      pagination: false,
      items: 5,
      itemsDesktop: [5000, 5],
      itemsDesktopSmall: [1440, 5],
      itemsTablet: [1024, 4],
      itemsTabletSmall: [640, 3],
      itemsMobile: [360, 3],
      autoHeight: false,
      autoPlay: false,
      navigation: false,
      responsiveBaseWidth: '#mastwrap',
      itemFixWidth: 100
    });
    $("#home-carousel-index").owlCarousel({
      pagination: false,
      items: 1,
      itemsDesktop: [5000, 1],
      itemsDesktopSmall: [1440, 1],
      itemsTablet: [1024, 1],
      itemsTabletSmall: [640, 1],
      itemsMobile: [360, 1],
      autoHeight: true,
      autoPlay: false
    });

    //MediaFolio Engine
    $('.mediafolio-thumb-wrap').mixitup();
    $('.mediafolio-filter li').click(function () {
      $('.mediafolio-filter li').removeClass('triggered');
      $(this).addClass('triggered');
    });
  });
})();