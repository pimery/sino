from django.shortcuts import render, redirect
from django.views.generic import *

from flow.models import *


class FlowView(DetailView):
    model = Node
    template_name = 'detail_flow.html'

    def get_context_data(self, **kwargs):
        context = super(FlowView, self).get_context_data(**kwargs)
        if Arrow.objects.filter(up=self.get_object()).exists():
            context['arrows'] = Arrow.objects.filter(up=self.get_object())
        return context


class FlowList(ListView):
    model = Node
    template_name = 'flow.html'

    def get_queryset(self):
        return Node.objects.filter(is_top=True)

    def dispatch(self, request, *args, **kwargs):
        if self.get_queryset().count() == 1:
            return redirect('flow', pk=self.get_queryset()[0].id)
        return super(FlowList, self).dispatch(request, *args, **kwargs)